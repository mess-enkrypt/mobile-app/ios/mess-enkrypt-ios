//
//  ModelClass.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-06-05.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import Foundation

class ModelClass {
    internal let session = URLSession.shared
    internal let devUrl = "https://api.dev.messenkrypt.xyz"
    //internal let baseUrl = "localhost:3000"
    
    internal func initPostRequest(url: String) -> URLRequest {
        let urlFormat = URL(string: devUrl + url)!
        
        var request = URLRequest(url: urlFormat)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return request
    }
    
   /* private func initGetRequest(url: String) -> URLRequest {
        return request
    }*/
}
