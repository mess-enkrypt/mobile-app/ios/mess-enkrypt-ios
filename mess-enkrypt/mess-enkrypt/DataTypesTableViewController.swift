//
//  DataTypesTableViewController.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-05-28.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

struct Group {
    var groupName: String
    var shortDesc: String
}
