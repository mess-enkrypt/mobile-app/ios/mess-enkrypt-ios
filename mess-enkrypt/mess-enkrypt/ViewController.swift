//
//  ViewController.swift
//  mess-enkrypt
//
//  Created by Nicolas Cartier on 07/05/2019.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit
import CoreData
import LocalAuthentication

class ViewController: UIViewController {

    @IBOutlet weak var onActivationButton: UIButton!
    @IBOutlet weak var biometricsButton: UIButton!
    @IBOutlet weak var pseudoTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var userinfo: UserInfo.Response?
    var user: User? = nil
    
    var isCreated = false
    var isCalled = 0
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let passManager = PasswordManager()
    let biometrics = BiometricIDAuth()
    
    private let indicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        self.view.addGestureRecognizer(tapGesture)
        
        biometricsButton.isHidden = !biometrics.canEvaluatePolicy()
        
        let bio = biometrics.biometricType()
        if (bio == "none") {
            biometricsButton.isHidden = true
        } else {
            biometricsButton.isHidden = false
            let image = UIImage(named: bio) as UIImage?
            biometricsButton.frame = CGRect(x: 100, y: 100, width: 50, height: 50)
            biometricsButton.setImage(image, for: UIControl.State.normal)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        SocketIOManager.sharedInstance.onLoginErr(completionHandler: {(data) -> Void in
            DispatchQueue.main.async {
                if (data == "USER_NOT_ACTIVE") {
                    self.deleteAllData()
                }
            }
        })
        
        SocketIOManager.sharedInstance.onLoginSuccess(completionHandler: {() -> Void in
            DispatchQueue.main.async {
                if self.isCreated == false {
                    self.performSegue(withIdentifier: "postRegistrationSegue", sender: self)
                }
            }
        })
                
        SocketIOManager.sharedInstance.onRegistration(completionHandler: {(data) -> Void in
            let json = data[0] as! [String: String]
            self.userinfo = UserInfo.Response(msg: "", username: json["username"]!, publicKey: json["publicKey"]!, passKey: json["destroyPassphrase"]!)
            
            self.passManager.storePassword(user: self.userinfo!.username, pass: self.passwordTextField.text!)
            
            let alert = UIAlertController(
                title: "Remote destroy pass: MUST BE SAVED",
                message: "You should make a screenshot now",
                preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Done",
                                          style: .default,
                                          handler: { [weak alert] (action) -> Void in
                                            self.performSegue(withIdentifier: "postRegistrationSegue",sender: self)}))
            
            let img = QRCodeGenerator.instance.generate(content: self.userinfo!.passKey)
            let imageView = UIImageView(frame: CGRect(x: 10, y: 120, width: 250, height: 250))
            
            imageView.image = img
            
            alert.addAction(UIAlertAction(title: "Save",
                                          style: .default,
                                          handler: {[weak alert] (action) -> Void in
                                            // TAKING SCREENSHOT
                                            UIGraphicsBeginImageContext(imageView.frame.size)
                                            imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
                                            let out = UIGraphicsGetImageFromCurrentImageContext()
                                            UIGraphicsEndImageContext()
                                            
                                            UIImageWriteToSavedPhotosAlbum(out!, nil, nil, nil)
                                            self.performSegue(withIdentifier: "postRegistrationSegue", sender: self)
            }))
            
            alert.view.addSubview(imageView)
            
            let height = NSLayoutConstraint(item: alert.view!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 450)
            let width = NSLayoutConstraint(item: alert.view!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 270)
            alert.view.addConstraint(height)
            alert.view.addConstraint(width)
            
            self.present(alert, animated: true, completion: nil)

        })
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request = NSFetchRequest<User>(entityName: "User")
        
        request.predicate = NSPredicate(format: "privateKey != %@", "")
        
        do {
            let result = try context.fetch(request)
            if result.count >= 1
            {
                isCreated = true
                user = result[result.count - 1]
                pseudoTextField.text = user!.name
                pseudoTextField.isEnabled = false
                pseudoTextField.textColor = .lightGray
                PGPManager.instance.fetchKeyPair(user: user!)
                biometrics.authenticateUser() { [weak self] in
                    self?.performSegue(withIdentifier: "postRegistrationSegue", sender: self)
                }
            } else {
                biometricsButton.isHidden = true
            }
        } catch {
            print("fetching user failed")
        }
    }
    
    @objc func viewTapped() {
        if (self.pseudoTextField.isEditing == true) {
            self.pseudoTextField.endEditing(true)
        }
    }
    
    // register button action
    @IBAction func buttonPressed(_ sender: UIButton) {
        showSpinner()
        var pseudo = pseudoTextField.text
        let password = passwordTextField.text
        
        if user != nil && user?.name == pseudo {
            if passManager.checkPassword(user: pseudo!, pass: password!) == true {
                SocketIOManager.sharedInstance.authToSocket(username: pseudo!)
            } else {
                passwordTextField.text = ""
                passwordTextField.placeholder = "Wrong password"
                passwordTextField.backgroundColor = .red
            }
        } else {
        
            if pseudo == nil || pseudo == "" {
                pseudoTextField.backgroundColor = .red
                return
            }
            if password == nil || password == "" {
                passwordTextField.backgroundColor = .red
                return
            }
            
            pseudo = pseudo!.trimmingCharacters(in: .whitespacesAndNewlines)
            SocketIOManager.sharedInstance.registerUser(username: pseudo!)
            self.isCreated = true
        }
        hideSpinner()
    }
    
    // prepare a packet for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if user == nil || user?.name != pseudoTextField?.text {
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            
            let request = NSFetchRequest<User>(entityName: "User")
            request.predicate = NSPredicate(format: "name= %@", userinfo!.username)
            do {
                let result = try context.fetch(request)
                if result.count == 0 {
                    let usr = User(context: context)
                    usr.name = userinfo?.username
                    usr.publicKey = userinfo?.publicKey
                    usr.privateKey = PGPManager.instance.getPrivateKey()
                    usr.keyPass = userinfo?.passKey
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    user = usr
                } else {
                    user = result[0]
                }
            } catch {
                print("fetching user failed")
            }
        }
        
        SocketIOManager.sharedInstance.getMessage(currentUsername: (user?.name!)!, completionHandler: { (messageInfo) -> Void in
            DispatchQueue.main.async { () -> Void in
                let dataCoreManager = CoreDataManager(delegate: self.appDelegate)
                if (self.viewIfLoaded?.window != nil) {
                    dataCoreManager.updateGroupsAndMessage(data: messageInfo)
                }
            }
        })
        
        if segue.identifier == "postRegistrationSegue" {
            if let destination = segue.destination as? UITabBarController {
                if let navigation = destination.viewControllers?[0] as? UINavigationController {
                    if let destVC = navigation.topViewController as? HomeTableViewController {
                        destVC.user = user
                        destVC.authenticatedUser = user?.name
                        if self.isCreated == true && isCalled == 0 {
                            SocketIOManager.sharedInstance.authToSocket(username: user!.name!)
                            isCalled += 1
                        } else if isCalled > 0 {
                            return
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onBiometricsButton(_ sender: Any) {
        biometrics.authenticateUser() { [weak self] in
            self?.performSegue(withIdentifier: "postRegistrationSegue", sender: self)
        }
    }
    
    private func    deleteAllData() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let coreData = CoreDataManager(delegate: appDelegate)
        
        coreData.deleteAllData()
        
        pseudoTextField.text = ""
        pseudoTextField.isEnabled = true
        pseudoTextField.textColor = .black
        passwordTextField.text = ""
        
        let alert = UIAlertController(title: "Destroyed", message: "Your account has been destroyed", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func    showSpinner() {
        indicator.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        indicator.backgroundColor = .darkGray
        indicator.center = view.center
        indicator.hidesWhenStopped = true
        
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    private func    hideSpinner() {
        indicator.stopAnimating()
    }
}
