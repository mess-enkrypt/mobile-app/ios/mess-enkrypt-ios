//
//  ProfileViewController.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-09-20.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    var currentUser: User?
    
    private var passKey: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

/*
        CODE TO CIRCLE A PICTURE
        
        profilePic.layer.masksToBounds = false
        profilePic.layer.cornerRadius = profilePic.frame.size.width / 2
        profilePic.clipsToBounds = true
*/
        
        profilePic.image = QRCodeGenerator.instance.generate(content: currentUser!.name!)
        
        userName.text = currentUser?.name
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var core = CoreDataManager(delegate: appDelegate)
        
        let usr = core.fetchUser(username: currentUser!.name!)
        passKey = usr?.keyPass
    }
    

    @IBAction func showDestroyCode(_ sender: Any) {
        let alert = UIAlertController(title: "Remote destroy pass: Scan this with another phone to destroy this account", message: "destroy code", preferredStyle: .alert)
         
        let doneAction = UIAlertAction(title: "Done", style: .default)
        
        let img = QRCodeGenerator.instance.generate(content: self.passKey!)
        let imageView = UIImageView(frame: CGRect(x: 10, y: 120, width: 250, height: 250))
        
        imageView.image = img
        
        alert.addAction(doneAction)
        alert.view.addSubview(imageView)
        
        let height = NSLayoutConstraint(item: alert.view!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 450)
        let width = NSLayoutConstraint(item: alert.view!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 270)
        alert.view.addConstraint(height)
        alert.view.addConstraint(width)
        
        self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
