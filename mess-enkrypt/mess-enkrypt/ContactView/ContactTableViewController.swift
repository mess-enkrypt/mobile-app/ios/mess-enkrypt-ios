//
//  ContactTableViewController.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-09-20.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit

class ContactTableViewController: UITableViewController, ContactCellDelegate {

    private var userList: [User]? = nil
    private var coreDataManager: CoreDataManager?
    private var clickerID: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.tableFooterView = UIView()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        coreDataManager = CoreDataManager(delegate: appDelegate)
        
        SocketIOManager.sharedInstance.onPublicKey(completionHandler: { (user) -> Void in
            DispatchQueue.main.async { () -> Void in
                let info = UserInfo.Response(msg: "", username: user["username"]!, publicKey: user["publicKey"]!, passKey: "")
                var message = info.username + " is already in your contacts"
                
                if (info.publicKey.contains("BCPG")) {
                    message = "Failed: Cannot add android users yet"
                } else if (self.coreDataManager?.fetchUser(username: info.username) == nil) {
                    let usr = self.coreDataManager!.addUser(data: info)
                    message = info.username + " has been added to your contacts"
                    self.userList?.append(usr)
                    self.tableView.reloadData()
                }
                
                let alert = UIAlertController(
                    title: "Add contact",
                    message: message,
                    preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok",
                                              style: .default,
                                              handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
        })
    }

    override func viewDidAppear(_ animated: Bool) {
        userList = coreDataManager?.fetchAllUsers()
        userList?.remove(at: 0)
        tableView.reloadData()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return userList != nil ? userList!.count : 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as? ContactTableViewCell else {
            fatalError("Dequeuing another type of cell, expected: ContactTableViewCell")
        }

        cell.delegate = self
        cell.userID.text = userList?[indexPath.row].name
        cell.fastButton.tag = indexPath.row
        
        return cell
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == .delete {
            coreDataManager?.deleteUser(username: (userList?[indexPath.row].name)!)
            userList?.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fastChatSegue" {
            let ctrl = segue.destination as! GroupAddTableViewController
            let s = sender as! UIButton
            ctrl.currentUser = coreDataManager?.fetchCurrentUser()
            ctrl.addUserCell(username: (userList?[s.tag].name)!)
        }
    }
    
    @IBAction func addContactButton(_ sender: Any) {
        let alert = UIAlertController(
                        title: "Add new contact",
                        message: "Enter a username or scan the QRCode",
                        preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.placeholder = "username"
        }
        
        alert.addAction(UIAlertAction(title: "QRCode",
                                      style: .default,
                                      handler: { [weak alert] (action) -> Void in
                                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeReaderViewController") as! QRCodeReaderViewController
                                        controller.completionHandler = { text in
                                            SocketIOManager.sharedInstance.askPublicKey(user: text)
                                            controller.dismiss(animated: true, completion: nil)
                                        }
                                        self.navigationController?.pushViewController(controller, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Ok",
                                      style: .default,
                                      handler: { [weak alert] (_) in
                                        let textField = alert?.textFields![0]
                                        SocketIOManager.sharedInstance.askPublicKey(user: textField!.text!)
                                      }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func callSegueFromCell(data: Any) {
        self.performSegue(withIdentifier: "fastChatSegue", sender: data)
    }
}

protocol ContactCellDelegate {
    func callSegueFromCell(data: Any)
}
