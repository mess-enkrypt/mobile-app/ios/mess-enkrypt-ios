//
//  ContactTableViewCell.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-09-20.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    
    var delegate: ContactCellDelegate!
    @IBOutlet weak var userID: UILabel!
    @IBOutlet weak var fastButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onFastTapped(_ sender: Any) {
        delegate.callSegueFromCell(data: sender)
    }
}
