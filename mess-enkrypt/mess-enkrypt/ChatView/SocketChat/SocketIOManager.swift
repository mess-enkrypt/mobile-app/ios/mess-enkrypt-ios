//
//  SocketIOManager.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-08-22.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit
import SocketIO

class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
    
    let devUrl = "http://api.dev.messenkrypt.xyz:1337"

    var socket: SocketIOClient!
    var manager: SocketManager = SocketManager(socketURL: URL(string: "https://api.dev.socket.messenkrypt.xyz")!)
    
 //   var manager: SocketManager = SocketManager(socketURL: URL(string: "http://192.168.1.78:3000")!)
    
    private var username: String = ""
    private var isAuth: Bool = false
    
    override init() {
        super.init()
        
        socket = manager.defaultSocket
        
        self.addHandlers()
    }
    
    func connectSocket() {
        socket.connect()
    }
    
    func closeSocket() {
        socket.disconnect()
    }
    
    func registerUser(username: String) {
        // TODO: Implement register through sockets
        
        let key = PGPManager.instance.createKeyPair(user: username)
        socket.emit("user:register", username, key)
    }
    
    func onRegistration(completionHandler: @escaping (_ data: [NSDictionary]) -> Void) {
        socket.on("user:register") { data, ack in
            completionHandler(data as! [NSDictionary])
        }
    }
    
    func askAuth(completionHandler: @escaping () -> Void) {
        socket.on("user:askauth") { data, ack in
            completionHandler()
        }
    }
    
    func loginUser(username: String) {
        if isAuth == false {
            socket.emit("user:login", username)
        }
    }
    
    func authToSocket(username: String) {
        self.username = username
        loginUser(username: username)
    }
    
    func sendMessage(group: Group, msg: String, sender: String, creator: User) {
        let users = getUserList(group: group, sender: sender)
        let keys = getKeysForUsers(group: group, sender: sender, creator: creator)
        
        for user in users {
            let jsonBody = createSendJson(group: group, msg: msg, keys: keys, sender: sender, to: user)
            socket.emit("message:send", user, jsonBody)
        }
    }
    
    func createGroup(group: GroupTypeFormat, userList: [String], current: User, groupObj: Group) {
        let keys = getKeysForUsers(group: groupObj, sender: current.name!, creator: current)
        
        let grouptype = createAddGroupJson(grp: group, keys: keys)
        for user in userList {
            socket.emit("message:send", user, grouptype)
        }
    }
    
    func getMessage(currentUsername: String, completionHandler: @escaping (_ messageInfo: Receive.Response) -> Void) {
        socket.on("message:receive") {data, ack in
            var jsonArray: Receive.Response = Receive.Response(msg: [])
            if let dataArray = data as? [String] {
                let decoder = JSONDecoder()
                do {
                    for elem in dataArray {
                        var json = try decoder.decode(MessageFormat.self, from: elem.data(using: .utf8)!)
                        json.msg = PGPManager.instance.decryptMessage(msg: json.msg)
                        jsonArray.msg.append(json)
                    }
                } catch let error {
                    print(error)
                }
            }
            completionHandler(jsonArray)
        }
    }
    
    func destroyUser(toDestroy: String, passphrase: String) {
        socket.emit("user:destroy", toDestroy, passphrase)
    }

    func askPublicKey(user: String) {
        socket.emit("user:getPublicKey", user)
    }
    
    func onLoginSuccess(completionHandler: @escaping () -> Void) {
        socket.on("user:login") { data, ack in
            if (self.isAuth == true) {
                completionHandler()
            }
        }
    }
    
    func onLoginErr(completionHandler: @escaping (_ data: String) -> Void) {
        socket.on("user:login:err") { data, ack in
            completionHandler(data[0] as! String)
        }
    }
    
    func onDestroySuccess(completionHandler: @escaping (_ data: String) -> Void) {
        socket.on("user:destroy:success") { data, ack in
            completionHandler(data[0] as! String)
        }
    }
    
    func onDestroy(completionHandler: @escaping () -> Void) {
        socket.on("user:isDestroyed") { data, ack in
            completionHandler()
        }
    }
    
    func onDestroyErr(completionHandler: @escaping (_ data: String) -> Void) {
        socket.on("user:destroy:err") { data, ack in
            completionHandler(data[0] as! String)
        }
    }
    
    func onSocketResponse(completionHandler: @escaping () -> Void) {
        socket.on("info") {data, ack in
            completionHandler()
        }
    }
    
    func onPublicKey(completionHandler: @escaping (_ data: [String:String]) -> Void) {
        socket.on("user:getPublicKey") { data, ack in
            let d = data[0] as! [String: String]
            completionHandler(d)
        }
    }
    
    func addHandlers() {
        socket.on("connect") { data, ack in
            if (self.username != "") {
                self.loginUser(username: self.username)
            }
        }

        socket.on("user:auth") { data, ack in
            self.isAuth = true
        }

        socket.on("user:login") { data, ack in
            if (self.isAuth == false) {
                guard let dict = data[0] as? [String: Any] else { return }
                //var msg = PGPManager.instance.decryptMessage(msg: dict["data"] as! String)
                //temporary the next line should be removed when server updates challenge
                var msg = "lol"
                self.isAuth = true
                self.socket.emit("user:auth", msg, self.username)
            }
        }
    }
    
    func userDisconnects() {
        self.isAuth = false
    }
    
    private func createSendJson(group: Group, msg: String, keys: [String], sender: String, to: String) -> String
    {
        let msgEnc = PGPManager.instance.encryptMessage(msg: msg, keys: keys)
        let nest = MessageTypeFormat(type: 0, content: msgEnc, grpName: group.name!, grpId: Int(group.gid), sender: sender)
        
        let jsonD = try? JSONEncoder().encode(nest)
        let encod = String(data: jsonD!, encoding: .utf8)
        let encrypted = PGPManager.instance.encryptMessage(msg: encod!, keys: keys)
        
        return encrypted
        
    }
    
    private func createAddGroupJson(grp: GroupTypeFormat, keys: [String]) -> String {
        let encodedGrp = (try? JSONEncoder().encode(grp))!
        let str = String(decoding: encodedGrp, as:UTF8.self).replacingOccurrences(of: "\\", with: "")
        
        let encrypted = PGPManager.instance.encryptMessage(msg: str, keys: keys)

        return encrypted
    }
    
    private func getKeysForUsers(group: Group, sender: String, creator: User) -> [String] {
        let users = group.user?.allObjects as! [User]
        
        var userKeys: [String] = [creator.publicKey!]
        
        for user in users {
            if user.name != sender {
                userKeys.append(user.publicKey!)
            }
        }
        
        return userKeys
    }
    
    private func getUserList(group: Group, sender: String) -> [String] {
        let users = group.user?.allObjects as! [User]
        var userList: [String] = []
        
        for user in users {
            if user.name != sender {
                userList.append(user.name!)
            }
        }
        
        if group.creator != sender && !userList.contains(group.creator!){
            userList.append(group.creator!)
        }
        
        return userList
    }
    
    private func    getDateNowFormatted() -> Date {
        let date = Date()
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        
        let str = dateFormat.string(from: date)
        return dateFormat.date(from: str)!
    }
}
