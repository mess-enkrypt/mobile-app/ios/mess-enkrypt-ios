//
//  ChatViewController.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-06-03.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var InputViewController: UIView!
    @IBOutlet weak var inputMessageField: UITextField!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var inputContainerHeightConstraint: NSLayoutConstraint!
    
    fileprivate let cellId = "id123"
    
    private var coreDataManager: CoreDataManager?
    private var publicKeys: [String]?
    private var keyboardH: CGFloat = 50.0
    
    var timer = Timer()
    
    var messages: [Message]? = []
    var currentGroup: Group?
    var currentUser: User?
    
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillHideNotification, object: nil)

        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Send"
        inputMessageField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(onSendTapped(_:)))
    }

    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Send"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (hasNotch) {
            self.inputContainerHeightConstraint.constant += 33.0
        }
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.inputMessageField.delegate = self
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        coreDataManager = CoreDataManager(delegate: appDelegate)
        
        currentUser = coreDataManager?.fetchCurrentUser()
        messages = coreDataManager?.fetchMessage(group: currentGroup!)
        
        let keys = currentGroup?.user?.sortedArray(using: [NSSortDescriptor(key: "name", ascending: true)]) as! [User]
        for usr in keys {
            publicKeys?.append(usr.publicKey!)
        }
 
        tableView.register(ChatMessageCell.self, forCellReuseIdentifier: cellId)
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor(white: 0.95, alpha: 1)

        //tap gesture recognizer on tableview
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tableViewTapped))
        self.tableView.addGestureRecognizer(tapGesture)
        
        navigationItem.title = currentGroup?.name
        
        SocketIOManager.sharedInstance.getMessage(currentUsername: currentUser!.name!, completionHandler: { (messageInfo) -> Void in
            DispatchQueue.main.async { () -> Void in
                self.coreDataManager?.updateGroupsAndMessage(data: messageInfo)
                self.messages = self.coreDataManager?.fetchMessage(group: self.currentGroup!)
                self.tableView.reloadData()
                
            }
        })
        
        SocketIOManager.sharedInstance.onSocketResponse(completionHandler: {() -> Void in
            DispatchQueue.main.async {
                self.messages = self.coreDataManager?.fetchMessage(group: self.currentGroup!)
                self.tableView.reloadData()
            }
        })
    }
    
    // Action method called when send button is tapped
    @IBAction func onSendTapped(_ sender: UIButton) {
        let creator = coreDataManager?.fetchUser(username: currentGroup!.creator!)
        
        if let text = inputMessageField.text {
            // chatViewModel?.sendMessage(group: currentGroup!, msg: text, sender: currentUser!.name!, creator: creator!)
            SocketIOManager.sharedInstance.sendMessage(group: currentGroup!, msg: text, sender: currentUser!.name!, creator: creator!)
            let nmsg = coreDataManager?.addMessage(group: currentGroup!, content: text, sender: currentUser!.name!)
            self.inputMessageField.text = nil
            self.inputMessageField.placeholder = "Type message here..."
            self.messages?.append(nmsg!)
            self.tableView.reloadData()
        }
    }

    func endRefreshGroup()
    {
        let grp = self.coreDataManager?.fetchGroup(id: currentGroup!.gid)
        self.messages = grp?.messages?.array as? [Message]
        
        self.tableView.reloadData()
        self.animateScrollDown()
    }
    
    @objc func tableViewTapped() {
        // force end editing text field
        self.inputMessageField.endEditing(true)
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        if let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if (keyboardH <= 50.0) {
                keyboardH = keyboardRect.height + 100
            } else {
                keyboardH = 50.0
            }
        }
    }
    
    // MARK: Text field delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.inputContainerHeightConstraint.constant = self.keyboardH - 50.0
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        animateScrollDown()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.inputContainerHeightConstraint.constant = self.keyboardH
            if (self.hasNotch) {
                self.inputContainerHeightConstraint.constant += 33.0
            }
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        animateScrollDown()
    }
    
    // MARK: Table View Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return messages?.count ?? 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ChatMessageCell
        
        if indexPath.row > 0 {
            if (messages?[indexPath.row - 1].from == messages?[indexPath.row].from)
            {
                cell.nameLabel.isHidden = true
            } else {
                cell.nameLabel.isHidden = false
            }
        }
        
        if messages != nil && messages?.count != 0 {
            let chatMessage = messages?[indexPath.row]

            cell.isIncoming = chatMessage?.from != currentUser?.name ? true : false
            cell.chatMessage = chatMessage
        }
        return cell
    }
    
    private func animateScrollDown() {
        if messages != nil && messages!.count > 0 {
            let indexPath = IndexPath(row: messages!.count - 1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
}
