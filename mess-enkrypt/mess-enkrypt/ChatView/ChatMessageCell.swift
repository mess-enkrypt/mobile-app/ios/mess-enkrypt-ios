//
//  ChatMessageCellTableViewCell.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-06-03.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit

class ChatMessageCell: UITableViewCell {

    let nameLabel = UILabel()
    let messageLabel = UILabel()
    let dateLabel = UILabel()
    let bubbleBackgroundView = UIView()
    
    var leadingConstraint: NSLayoutConstraint!
    var trailingConstraint: NSLayoutConstraint!
    
    var isIncoming: Bool?
    
    var chatMessage: Message! {
        didSet {
            bubbleBackgroundView.backgroundColor = isIncoming! ? .white : .darkGray
            messageLabel.textColor = isIncoming! ? .black : .white
            messageLabel.text = chatMessage.msg

            nameLabel.text = chatMessage.from
            nameLabel.textColor = .gray
            
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
            format.timeZone = TimeZone.current
            format.dateStyle = .short
            format.timeStyle = .short

            dateLabel.text = format.string(from: chatMessage.date!)
            dateLabel.textColor = .gray
            
            if isIncoming! {
                leadingConstraint.isActive = true
                trailingConstraint.isActive = false
            } else {
                leadingConstraint.isActive = false
                trailingConstraint.isActive = true
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        nameLabel.numberOfLines = 0
        nameLabel.font = UIFont(name: nameLabel.font.fontName, size: 14)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(nameLabel)

        dateLabel.numberOfLines = 0
        dateLabel.font = UIFont(name: nameLabel.font.fontName, size: 10)
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(dateLabel)

        
        backgroundColor = .clear
        
        bubbleBackgroundView.backgroundColor = .gray
        bubbleBackgroundView.layer.cornerRadius = 12
        addSubview(bubbleBackgroundView)
        bubbleBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(messageLabel)
        messageLabel.numberOfLines = 0
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        
        // constraints
        let messageBubbleConstraints = [

        nameLabel.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -16),
        nameLabel.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 16),
        nameLabel.bottomAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -20),
        
        dateLabel.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -16),
        dateLabel.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 16),
        dateLabel.bottomAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 32),
        
        messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: 32),
        messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -32),
        messageLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 260),
        
        bubbleBackgroundView.topAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -16),
        bubbleBackgroundView.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -16),
        bubbleBackgroundView.bottomAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 16),
        bubbleBackgroundView.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 16),
        ]
        
        NSLayoutConstraint.activate(messageBubbleConstraints)
    
        leadingConstraint = messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32)
        leadingConstraint.isActive = false
        
        trailingConstraint = messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32)
        trailingConstraint.isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }}
