//
//  ChatViewCoreDataManager.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-06-07.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import Foundation
import CoreData

class   ChatViewCoreDataManager {
    private var appDelegate: AppDelegate?
    private var context: NSManagedObjectContext?

    private var groupUUID: String?
    
    init(uuid: String, delegate: AppDelegate) {
        groupUUID = uuid
        appDelegate = delegate
        context = appDelegate?.persistentContainer.viewContext
    }
    
    func fetchMessageForGroup() -> [Message]? {
        let request = NSFetchRequest<Group>(entityName: "Group")
        
        request.predicate = NSPredicate(format: "uuid == %@", groupUUID!)
        
        do {
            let result = try context!.fetch(request)
            if result.count == 1 {
                return result[0].messages?.array as? [Message]
            }
        } catch {
            print("ChatViewCoreDataManager fetching Group content failed")
        }
        return []
    }
    
    func storeSentMessageOnGroup(value: Send.Response) {
        let request = NSFetchRequest<Group>(entityName: "Group")
        
        request.predicate = NSPredicate(format: "uuid == %@", groupUUID!)
        
        do {
            let result = try context!.fetch(request)
            if result.count == 1 {
                let msg = Message(context: context!)
                
                let dateFormater = DateFormatter()
                
                dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
                
                let finDate = dateFormater.date(from: value.msg.date)
                
                msg.id = value.msg._id
                msg.date = finDate
                msg.from = value.msg.from
                msg.to = value.msg.to
               
               // let decrypt = PGPManager.instance.decryptMessage(msg: value.msg.msg)
                msg.msg = value.msg.msg
                
                result[0].addToMessages(msg)
                result[0].date = msg.date
                msg.group = result[0]
            }
        } catch {
            print("ChatViewCoreDataManager fetching Group content failed")
        }
        appDelegate?.saveContext()
    }
    
    func deleteGroupFromDB(name: String) {
        let request = NSFetchRequest<Group>(entityName: "Group")
        
        request.predicate = NSPredicate(format: "from == %@", name)
        do {
            if let result = try context?.fetch(request) {
                for obj in result {
                    context?.delete(obj)
                }
            }
        } catch {
            print("Asked to delete a non existing group")
        }
    }
}
