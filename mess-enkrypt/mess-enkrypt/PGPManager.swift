//
//  PGPManager.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-06-09.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import Foundation
import ObjectivePGP

class PGPManager {
    // Implementing singleton
    static let instance = PGPManager()
    
    var publicKey: Key?
    var privateKey: Key?
    
    func    createKeyPair(user: String) -> String {
        
        let key = KeyGenerator().generate(for: user, passphrase: nil)
        
        do {
            let pub = try key.export(keyType: .public)
            let priv = try key.export(keyType: .secret)
            
            try publicKey = ObjectivePGP.readKeys(from: pub)[0]
            try privateKey = ObjectivePGP.readKeys(from: priv)[0]
            
            let keyring = ObjectivePGP.defaultKeyring
            keyring.import(keys: [publicKey!, privateKey!])
            
            return getPublicKey()
        } catch {
            print("generate keys failed")
        }
        return ""
    }
    
    func    fetchKeyPair(user: User){
        do {
            publicKey = try ObjectivePGP.readKeys(from: (user.publicKey?.data(using: .utf8))!)[0]
            privateKey = try ObjectivePGP.readKeys(from: (user.privateKey?.data(using: .utf8))!)[0]
        }
        catch {
            print("Fetching keys failed")
        }
    }
    
    func getPrivateKey() -> String {
        return retrieveKey(keyID: (privateKey?.secretKey?.keyID.shortIdentifier)!, type: .secretKey)
    }
    
    func getPublicKey() -> String {
        return retrieveKey(keyID: (publicKey?.publicKey?.keyID.shortIdentifier)!, type: .publicKey)
    }
    
    func encryptMessage(msg: String, keys: [String]) -> String {
        do {
            var receivers: [Key] = []
            for key in keys {
                let k = try ObjectivePGP.readKeys(from: (key.data(using: .utf8)!))[0]
                receivers.append(k)
            }
            
            receivers.append(publicKey!)
            
            let encrypted = try ObjectivePGP.encrypt(msg.data(using: .utf8)!, addSignature: false, using: receivers)

            let messageStr = Armor.armored(encrypted, as: .message)
    
            return messageStr
        }
        catch {
            print("encryption failed")
        }
        return ""
    }

    func decryptMessage(msg: String) -> String {
        do {
            let decrypted = try ObjectivePGP.decrypt(msg.data(using: .utf8)!, andVerifySignature: false, using: [privateKey!])
            return String(decoding: decrypted, as: UTF8.self)
        } catch {
            print("decryption failed")
        }
        
        return ""
    }

    
    private func    retrieveKey(keyID: String, type: PGPArmorType) -> String {
        let keyring = ObjectivePGP.defaultKeyring
        
        if let key = keyring.findKey(keyID) {
            do {
                let pub = try key.export(keyType: type == .publicKey ? .public : .secret)
                let armored = Armor.armored(pub, as: type)
                return armored
            } catch {
                print("Could not retrieve public key")
            }
        }
        
        return ""
    }
}
