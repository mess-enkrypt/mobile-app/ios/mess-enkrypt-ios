//
//  AppDelegate.swift
//  mess-enkrypt
//
//  Created by Nicolas Cartier on 07/05/2019.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        SocketIOManager.sharedInstance.onDestroy(completionHandler: {() -> Void in
            self.deleteAllData()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let newRoot = storyboard.instantiateInitialViewController() else {
                return // This shouldn't happen
            }
            let alert = UIAlertController(title: "Destroyed", message: "Your account has been destroyed", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.window?.rootViewController = newRoot
            newRoot.present(alert, animated: true, completion: nil)
        }
        )
        
        return true
    }

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "MessageDB")
        
        container.loadPersistentStores(completionHandler: {(StoreDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved storage error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext() {
        let context = persistentContainer.viewContext
        
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved storage error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        SocketIOManager.sharedInstance.userDisconnects()
        SocketIOManager.sharedInstance.closeSocket()
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        let coredata = CoreDataManager(delegate: self)
        let users = coredata.fetchCurrentUser()
        
    //    let manager = PasswordManager()
    //   manager.displayAlert(user: (users?.name!)!, view: UIWindow.visibleViewController!)
        
        SocketIOManager.sharedInstance.authToSocket(username: (users?.name!)!)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        SocketIOManager.sharedInstance.connectSocket()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    private func    deleteAllData() {
        let coreData = CoreDataManager(delegate: self)
        
        coreData.deleteAllData()
                
    }

}
