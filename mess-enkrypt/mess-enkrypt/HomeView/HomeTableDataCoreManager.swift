//
//  HomeTableDataCoreManager.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-06-07.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import Foundation
import CoreData

class HomeTableCoreDataManager {
    private var username: String?
    private var appDelegate: AppDelegate?
    private var context: NSManagedObjectContext?
    
    init(name: String, delegate: AppDelegate) {
        username = name
        appDelegate = delegate
        context = appDelegate?.persistentContainer.viewContext
    }
    
    func    createGroup(receiver: String) -> Group {
        let group = Group(context: context!)
        
        group.from = receiver
        group.to = username
        group.uuid = UUID().uuidString
        
        return group
    }
    
    func    fetchGroupForUser() -> [Group]? {
        let requestUsr = NSFetchRequest<Group>(entityName: "Group")
        let dateSort = NSSortDescriptor(key: "date", ascending: false)
        
        requestUsr.predicate = NSPredicate(format: "to == %@", username!)
        requestUsr.sortDescriptors = [dateSort]
        
        do {
            let result = try context!.fetch(requestUsr)
            return result
        } catch {
            print("Hometableview coredata fetching groups failed")
        }
        return []
    }
    
    func    storeGroupForUser(data: Receive.Response?) {
        if data != nil {
            for msg in data!.msg {
                let request = NSFetchRequest<Group>(entityName: "Group")
                request.predicate = NSPredicate(format: "from == %@ AND to == %@", msg.from, username!)
                do {
                    var group: Group?
                    let result = try context!.fetch(request)
                    if result.count == 0 {
                        group = Group(context: context!)
                        group!.from = msg.from
                        group!.to = username
                        group!.uuid = UUID().uuidString
                    } else {
                        group = result[0]
                    }
                    
                    let text = createMessageFromReponse(msg: msg)
                    text.group = group
                    group!.date = text.date
                    group?.addToMessages(text)
                    
                } catch {
                    print("Hometableview coredata fetching failed")
                }
            }
            appDelegate?.saveContext()
        }
    }
    
    private func    createMessageFromReponse(msg: MessageFormat) -> Message{
        let text = Message(context: context!)
        
        let dateFormater = DateFormatter()
        
        dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        
        let finDate = dateFormater.date(from: msg.date)
        
        text.id = msg._id
        text.from = msg.from
        text.to = msg.to
        text.date = finDate
        text.msg = msg.msg!
        text.msg = PGPManager.instance.decryptMessage(msg: msg.msg!)

        return text
    }
    
}
