//
//  GroupNameCell.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-06-16.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit

class GroupNameCell: UITableViewCell {

    weak var delegate:  GroupNameCellDelegate?
    @IBOutlet weak var groupName: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func onGroupNameEdited(_ sender: Any) {
        if groupName.text != nil && groupName.text != "" {
            self.delegate?.storeGroupName(group: groupName.text!)
        } else {
            groupName.backgroundColor = .red
            groupName.placeholder = "Please enter a group name"
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

protocol    GroupNameCellDelegate : AnyObject {
    func    storeGroupName(group: String)
}
