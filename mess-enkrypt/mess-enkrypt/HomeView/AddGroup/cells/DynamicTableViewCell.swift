//
//  DynamicTableViewCell.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-06-15.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit

class DynamicTableViewCell: UITableViewCell {

    weak var delegate: DynamicTableViewCellDelegate?
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBAction func onAddButtonTapped(_ sender: Any) {
        if username.text != nil && username.text != ""
        {
            self.delegate?.addUserCell(username: username.text!)
            showCancel()
        }
    }
    
    @IBAction func onRemoveTapped(_ sender: Any) {
        self.delegate?.removeUserCell(username: username.text!)
        showAdd()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showAdd() {
        self.cancelButton.isHidden = true
        self.cancelButton.isEnabled = false
        
        self.username.isEnabled = true
        self.username.text = ""
        
        self.addButton.isEnabled = true
        self.addButton.isHidden = false
    }
    
    func showCancel() {
        self.addButton.isEnabled = false
        self.addButton.isHidden = true
        
        self.username.isEnabled = false
        
        self.cancelButton.isHidden = false
        self.cancelButton.isEnabled = true
    }

}

protocol DynamicTableViewCellDelegate: AnyObject {
    func    addUserCell(username: String)
    func    removeUserCell(username: String)
}
