//
//  ContactTableViewController.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-09-20.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit

class AddFromContactTableViewController: UITableViewController {
    
    private var userList: [User]? = nil
    private var selectedList: [String] = []
    private var coreDataManager: CoreDataManager?
    
    var delegate: PassUserList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        coreDataManager = CoreDataManager(delegate: appDelegate)
        userList = coreDataManager?.fetchAllUsers()
        userList?.remove(at: 0)
        
        selectedList = (delegate?.getUserList())!
    }
    
    override func viewDidAppear(_ animated: Bool) {
        userList = coreDataManager?.fetchAllUsers()
        userList?.remove(at: 0)
        
        selectedList = (delegate?.getUserList())!
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return userList != nil ? userList!.count : 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FromContactTableViewCell", for: indexPath) as? FromContactCellTableViewCell else {
            fatalError("Dequeuing another type of cell, expected: FromContactTableViewCell")
        }
        
        cell.username.text = userList?[indexPath.row].name
        
        if selectedList.contains((userList?[indexPath.row].name)!) {
            cell.switchElem.setOn(true, animated: true)
        }
        
        cell.switchElem.tag = indexPath.row
        
        return cell
    }
    
    @IBAction func valueChanged(_ sender: UISwitch) {
        if (selectedList.contains((userList?[sender.tag].name)!)) {
            selectedList.removeAll { $0 == (userList?[sender.tag].name)! }
            delegate?.removeUserInList(usr: (userList?[sender.tag].name)!)
        } else {
            selectedList.append((userList?[sender.tag].name)!)
        }
    }
    
    override func didMove(toParent parent: UIViewController?) {
        if (parent == nil) {
            delegate?.setUserList(usr: selectedList)
        }
        
    }
    
}

protocol PassUserList {
    func setUserList(usr: [String])
    func removeUserInList(usr: String)
    func getUserList() -> [String]
}
