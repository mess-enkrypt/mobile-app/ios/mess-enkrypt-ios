//
//  FromContactCellTableViewCell.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-11-01.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit

class FromContactCellTableViewCell: UITableViewCell {
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var switchElem: UISwitch!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
