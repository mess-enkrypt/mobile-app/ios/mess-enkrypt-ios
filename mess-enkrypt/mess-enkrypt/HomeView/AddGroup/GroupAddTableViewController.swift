//
//  GroupAddTableViewController.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-06-15.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit

class GroupAddTableViewController: UITableViewController, DynamicTableViewCellDelegate, PassUserList {
    
    var nbGroup: Int = 1
    var currentUser: User? = nil
    
    private let indicator = UIActivityIndicatorView()
    
    private var     groupName: String?
    private var     usersInGroup: [User] = []
    
    private let coreDataManager: CoreDataManager = CoreDataManager(delegate: UIApplication.shared.delegate as! AppDelegate)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SocketIOManager.sharedInstance.onPublicKey(completionHandler: { (user) -> Void in
            DispatchQueue.main.async { () -> Void in
                let info = UserInfo.Response(msg: "", username: user["username"]!, publicKey: user["publicKey"]!, passKey: "")
                
                if (info.publicKey.contains("BCPG")) {
                    let alert = UIAlertController(
                       title: "Add contact",
                       message: "Failed: Cannot add android users yet",
                       preferredStyle: .alert)
                   
                    alert.addAction(UIAlertAction(title: "Ok",
                                                 style: .default,
                                                 handler: nil))
                   
                    self.tableView.reloadData()
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let usr = self.coreDataManager.addUser(data: info)
                    self.usersInGroup.append(usr)
                        
                    self.nbGroup += 1
                    self.tableView.reloadData()
                }
                self.hideSpinner()
            }
        })
    }

    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    // MARK: - Pre segue operations
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if segue.identifier == "FromContactsSegue" {
            let dest = segue.destination as! AddFromContactTableViewController
            dest.delegate = self
       } else if segue.identifier == "beforeScanSegue" {
            let dest = segue.destination as! QRCodeReaderViewController
            dest.completionHandler = { text in
                self.addUserCell(username: text)            }
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        var retval = true
        var message = ""
        if identifier == "groupCreatedSegue" {
            if groupName == nil || groupName == "" {
                retval = false
                message = "Please set a group name"
            }
            if usersInGroup.count == 0 {
                retval = false
                message = "No users in group"
            }
        }
        if retval == false {
            let alert = UIAlertController(title: "ERROR", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        return retval
    }
    
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2 {
            return nbGroup
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "groupname", for: indexPath) as! GroupNameCell
            
            cell.delegate = self
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "static", for: indexPath)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dynamic", for: indexPath) as! DynamicTableViewCell
            
            cell.delegate = self
            if cell.username.text != "" && (usersInGroup.count == 0 || indexPath.row >= usersInGroup.count) {
                cell.showAdd()
            }
            else if cell.username.text != "" && usersInGroup[indexPath.row].name != cell.username.text {
                cell.showAdd()
            }
            if usersInGroup.count > indexPath.row {
                cell.username.text = usersInGroup[indexPath.row].name
                cell.showCancel()
            }
            return cell
        }
    }

    @IBAction func onCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onButtonDoneTapped(_ sender: Any) {
        let current = coreDataManager.fetchCurrentUser()!
        
        if (groupName != nil && groupName != "" && usersInGroup.count != 0) {
            var userArr: [String] = []
            for usr in usersInGroup {
                userArr.append(usr.name!)
            }
            
            var userlist = "["
            userlist += userArr.joined(separator: ",")
            userlist += "]"
            
            let group = GroupTypeFormat(type: 1, grpName: groupName!, groupPic: "", creator: current.name!, gid: UUID().hashValue, userlist: userlist)
            let groupObj = coreDataManager.addGroup(groupname: group.grpName, users: usersInGroup, current: group.creator, id: group.gid)

            SocketIOManager.sharedInstance.createGroup(group: group, userList: userArr, current: currentUser!, groupObj: groupObj)
            tableView.reloadData()
        } else {
            let alert = UIAlertController(title: "ERROR", message: "Please specify a group name and at least one user", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    private func    getDateNowFormatted() -> Date {
        let date = Date()
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        
        let str = dateFormat.string(from: date)
        return dateFormat.date(from: str)!
    }

    private func    showSpinner() {
        indicator.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        indicator.center = view.center
        indicator.hidesWhenStopped = true

        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    private func    hideSpinner() {
        indicator.stopAnimating()
    }

    func    removeUserCell(username: String) {
        let usr = coreDataManager.fetchUser(username: username)!

        if let idx = usersInGroup.firstIndex(of: usr) {
            usersInGroup.remove(at: idx)
            nbGroup -= 1
        }
        
        tableView.reloadData()
    }
    
    func    addUserCell(username: String) {
        let usr = coreDataManager.fetchUser(username: username)
        if usr == nil {
            self.showSpinner()
            SocketIOManager.sharedInstance.askPublicKey(user: username)
        }
        
        if usr != nil && !usersInGroup.contains(usr!) {
            DispatchQueue.main.async {
                self.usersInGroup.append(usr!)
                self.nbGroup += 1
                self.tableView.reloadData()
                self.hideSpinner()
            }
        }
    }
    
    func removeUserInList(usr: String) {
        removeUserCell(username: usr)
    }
    
    func setUserList(usr: [String]) {
        for user in usr {
            addUserCell(username: user)
        }
    }
    
    func getUserList() -> [String] {
        var usrList: [String] = []
        
        for user in usersInGroup {
            usrList.append(user.name!)
        }
        
        return usrList
    }
}

extension GroupAddTableViewController : GroupNameCellDelegate {
    func    storeGroupName(group: String) {
        self.groupName = group
    }
}
