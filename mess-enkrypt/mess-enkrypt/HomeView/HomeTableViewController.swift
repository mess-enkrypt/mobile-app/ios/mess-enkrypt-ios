//
//  HomeTableViewController.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-05-09.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit
import CoreData

class HomeTableViewController: UITableViewController {
    var authenticatedUser: String?
    
    var user: User?
    var chatGroup: [Group]?
    
    var nGroup: Group? = nil
    
    private var MessageList: Receive.Response?
    
    private var dataCoreManager: CoreDataManager?
        
    // cheap fix for refreshing on the messageView
    var segueHappened: Bool = false
    var nextController: ChatViewController? = nil
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        segueHappened = false
        nextController = nil
        
        user = dataCoreManager?.fetchUser(username: authenticatedUser!)
        chatGroup = dataCoreManager?.fetchAllGroups()
        sortGroups()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        dataCoreManager = CoreDataManager(delegate: appDelegate)
        if user == nil {
            user = dataCoreManager?.fetchUser(username: authenticatedUser!)
        }
        
        chatGroup = user?.group?.allObjects as? [Group]
                
        self.navigationItem.title = user?.name

        
        SocketIOManager.sharedInstance.getMessage(currentUsername: user!.name!, completionHandler: { (messageInfo) -> Void in
            DispatchQueue.main.async { () -> Void in
                if (self.viewIfLoaded?.window != nil) {
                    self.dataCoreManager?.updateGroupsAndMessage(data: messageInfo)
                    self.chatGroup = self.dataCoreManager?.fetchAllGroups()
                    self.tableView.reloadData()
                }
            }
        })
        
        SocketIOManager.sharedInstance.onSocketResponse(completionHandler: {() -> Void in
            DispatchQueue.main.async {
                self.chatGroup = self.dataCoreManager?.fetchAllGroups()
                self.sortGroups()
                self.tableView.reloadData()
            }
        })
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return chatGroup?.count ?? 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "HomeTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? HomeTableViewCell else {
            fatalError("The cell is not an instance of HomeTableViewCell")
        }
        
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        format.timeZone = TimeZone.current
        format.dateStyle = .short
        format.timeStyle = .short
        
        cell.groupName.text = chatGroup?[indexPath.row].name ?? "empty"
        cell.last.text = format.string(from: (chatGroup?[indexPath.row].date)!)

        let message = chatGroup?[indexPath.row].messages?.array as! [Message]
        if message.count >= 1 {
            if let content = message[message.count - 1].msg {
                cell.date.text = format.string(from: message[message.count - 1].date!)
                cell.last.text = content
            }
        } else {
            cell.last.text = ""
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "openGroupSegue" {
            let destination = segue.destination as! ChatViewController

            if nGroup == nil {
                if let indexPath = tableView.indexPathForSelectedRow {
                    destination.currentGroup = chatGroup![indexPath.row]
                }
            } else {
                destination.currentGroup = nGroup
                nGroup = nil
            }
        } else if segue.identifier == "addGroupSegue" {
            let destination = segue.destination as! GroupAddTableViewController
            destination.currentUser = user
        } else if segue.identifier == "profileSegue" {
            let destination = segue.destination as! ProfileViewController
            destination.currentUser = user
        }
    }
    
    //MARK: IBActions for buttons
    @IBAction func cancelAddGroup(_ segue: UIStoryboardSegue) {
    }
    
    private func    sortGroups() {
        chatGroup = user?.group?.allObjects as? [Group]
        chatGroup?.sort {
            let d1 = $0.messages!.array as? [Message]
            let d2 = $1.messages!.array as? [Message]
            
            if (d1!.count == 0 && d2!.count != 0) {
                return false
            }
            if (d1!.count != 0 && d2!.count == 0) {
                return true
            }
            else if (d1!.count == 0 && d2!.count == 0) {
                return false
            }
            
            return d1![d1!.count - 1].date!.compare(d2![d2!.count - 1].date!) == .orderedDescending
        }
        
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == .delete {
            dataCoreManager?.deleteGroup(group: chatGroup![indexPath.row].gid)
            /* userList?.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)*/
            tableView.reloadData()
        }
        
    }
}
