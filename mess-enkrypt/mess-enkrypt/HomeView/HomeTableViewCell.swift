//
//  HomeTableViewCell.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-05-09.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    //MARK: Properties

    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var last: UILabel!
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {

        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
