//
//  HomeViewTableViewCell.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-05-09.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit

class HomeViewTableViewCell: UITableViewCell {
    //Mark: Properties
    
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var shortDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
