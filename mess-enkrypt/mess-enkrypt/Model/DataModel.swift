//
//  DataModel.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-06-05.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import Foundation

// JSON format for send message request
struct Send {
    static let url: String = "/msg/send"
    
    struct Request : Codable {
        let from: String
        let to: String
        let msg: String
    }
    
    struct Response : Codable {
        let msg: MessageFormat
    }
}

// JSON format for receive message request
struct Receive {
    static let url: String = "/msg/receive"
    
    struct Request : Codable {
        let username: String
    }
    
    enum ResponseType {
        case Message
        case Group
    }
    
    
    struct Response : Codable {
        var msg: [MessageFormat]
    }
    
    struct MessageResponse : Codable {
        let msg: [MessageTypeFormat]
    }
    
    struct GroupResponse : Codable {
        let msg: [GroupTypeFormat]
    }
}

// JSON format for the scheme of a message
struct MessageFormat : Codable {
    let _id: String
    let from: String
    let to: String
    var msg: String
    let date: String
}

struct MessageTypeFormat : Codable {
    let type: Int
    let content: String
    let grpName: String
    let grpId: Int
    let sender: String
}

struct GroupTypeFormat : Codable {
    let type: Int
    let grpName: String
    let groupPic: String
    let creator: String
    let gid: Int
    let userlist: String
}

// JSON format for registration request
struct Register {
    static let url: String = "/user/register"
    
    struct Request : Codable {
        let username: String
        let publicKey: String
    }
    
    struct Response : Codable {
        let message: String
        let username: String
        let publicKey: String
        let destroyPassphrase: String
    }
    
}

// JSON format to get public key with username
struct UserInfo {
    static let urlUserName: String = "/user/get/username"
    static let urlPublicKey: String = "/user/get/publickey"
    
    struct RequestUserName : Codable {
        let publicKey: String
    }

    struct RequestPublicKey : Codable {
        let username: String
    }
    
    struct Response : Codable {
        let msg: String
        let username: String
        let publicKey: String
        let passKey: String
    }
}

// JSON formats to destroy users keys
struct DestroyKeys {
    static let url: String = "/user/destroy"
    
    struct Request : Codable {
        let username: String
        let destroyPassphrase: String
    }
    
    struct Response : Codable {
        let msg: String
    }
}
