//
//  LockAccountViewController.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-06-10.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class LockAccountViewController: UIViewController {

    @IBOutlet weak var userID: UITextField!
    @IBOutlet weak var pass: UITextView!
    @IBOutlet weak var lockButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        pass.layer.borderColor = UIColor.lightGray.cgColor
        userID.layer.borderColor = UIColor.lightGray.cgColor

        pass.layer.borderWidth = 1.0
        pass.layer.cornerRadius = 6
        IQKeyboardManager.shared.enable = true
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if segue.identifier == "beforeDestroyScan" {
            let dest = segue.destination as! QRCodeReaderViewController
            dest.completionHandler = { text in
                self.pass.text = text
            }
       } else if segue.identifier == "beforeUserScan" {
            let dest = segue.destination as! QRCodeReaderViewController
            dest.completionHandler = { text in
                self.userID.text = text
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        lockButton.addTarget(self, action: #selector(onLockPressed), for: .allTouchEvents)
    
        SocketIOManager.sharedInstance.onDestroySuccess(completionHandler: {(data) -> Void in
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "SUCCESS", message: "User has been successfully destroyed", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)            }
        })
        
        SocketIOManager.sharedInstance.onDestroyErr(completionHandler: { (data) -> Void in
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "ERROR", message: "Wrong passphrase", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    @objc func onLockPressed() {
        if userID.text == nil || userID.text!.isEmpty {
            userID.backgroundColor = .red
            return
        }
        if pass.text == nil || pass.text!.isEmpty {
            pass.backgroundColor = .red
            return
        }

        SocketIOManager.sharedInstance.destroyUser(toDestroy: userID.text!, passphrase: pass.text!)
        
        userID.text = ""
        pass.text = ""
    }
}
