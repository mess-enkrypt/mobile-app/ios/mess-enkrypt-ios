//
//  GroupDataCoreManager.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-06-15.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {
    private var context: NSManagedObjectContext?
    private var appDelegate: AppDelegate?
    
    init(delegate: AppDelegate) {
        appDelegate = delegate
        context = appDelegate!.persistentContainer.viewContext
        
        SocketIOManager.sharedInstance.onPublicKey(completionHandler: {(data) -> Void in
            var usr = self.fetchUser(username: data["username"]!)
            usr?.publicKey = data["publicKey"]
            self.appDelegate?.saveContext()
        })
    }

    func deleteAllData() {
        let entities = ["Group", "User", "Message"]
        
        for entity in entities {
            let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
            fetchReq.returnsObjectsAsFaults = false
            
            do {
                let result = try context!.fetch(fetchReq)
                for object in result {
                    guard let obj = object as? NSManagedObject else {continue}
                    context!.delete(obj)
                }
            } catch let error {
                fatalError(error.localizedDescription)
            }
        }
        appDelegate?.saveContext()
    }
    
    func deleteUser(username: String) {
        let request = NSFetchRequest<User>(entityName: "User")
        request.returnsObjectsAsFaults = false

        request.predicate = NSPredicate(format: "name == %@", username)
        
        do {
            let result = try context!.fetch(request)
            for object in result {
                guard let obj = object as? NSManagedObject else {continue}
                context!.delete(obj)
            }
        } catch let error {
            fatalError(error.localizedDescription)
        }
        
        appDelegate?.saveContext()
    }
    
    func    deleteGroup(group: Int32) {
        let request = NSFetchRequest<Group>(entityName: "Group")
        request.returnsObjectsAsFaults = false
        
        request.predicate = NSPredicate(format: "gid == %i", group)
        
        do {
            let result = try context!.fetch(request)
            for object in result {
                for message in object.messages! {
                    context!.delete(message as! NSManagedObject)
                }
            }
        } catch let error {
            fatalError(error.localizedDescription)
        }
        
        appDelegate?.saveContext()
    }
      
    
    //MARK: - Fetch methods
    
    func    fetchCurrentUser() -> User? {
        let request = NSFetchRequest<User>(entityName: "User")
        
        request.predicate = NSPredicate(format: "privateKey != %@", "")
        
        do {
            let result = try context!.fetch(request)
            if result.count == 1 {
                return result[0]
            }
            return nil
        } catch {
            print("fetch user failed")
        }
        return nil
    }
    
    func    fetchUser(username: String) -> User? {
        let request = NSFetchRequest<User>(entityName: "User")
        
        request.predicate = NSPredicate(format: "name == %@", username)
        
        do {
            let result = try context!.fetch(request)
            if result.count >= 1 {
                return result[0]
            }
            return nil
        } catch {
            print("fetch user failed")
        }
        return nil
    }
    
    func    fetchMessage(group: Group) -> [Message]? {
        let msg = group.messages?.array as! [Message]
        return msg
    }
    
    func    fetchMessageByID(id: String) -> Message? {
        let request = NSFetchRequest<Message>(entityName: "Message")
        
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let result = try context!.fetch(request)
            if result.count == 1 {
                return result[0]
            }
            return nil
        } catch {
            fatalError("Fetch by ID: an error occured")
        }
        
        return nil
    }

    func    fetchGroup(id: Int32) -> Group? {
        let request = NSFetchRequest<Group>(entityName: "Group")

        request.predicate = NSPredicate(format: "gid == %i", id)
        
        do {
            let result = try context!.fetch(request)
            if result.count >= 1 {
                return result[0]
            }
        } catch let error {
            print(error)
        }
        return nil
    }

    func    fetchAllGroups() -> [Group] {
        let request = NSFetchRequest<Group>(entityName: "Group")
        
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]

        do {
            let result = try context!.fetch(request)
            if result.count >= 1 {
                return result
            }
        } catch let error {
            print(error)
        }
        return []
    }
    
    func    fetchAllUsers() -> [User] {
        let request = NSFetchRequest<User>(entityName: "User")
        
        do {
            let result = try context!.fetch(request)
            if result.count >= 1 {
                return result
            }
        } catch let error {
            print(error)
        }
        return []
    }
    
    //MARK: - Add methods
    
    func    addUser(data: UserInfo.Response) -> User {
        let user = User(context: context!)
        
        user.name = data.username
        user.publicKey = data.publicKey
        
        appDelegate?.saveContext()
        return user
    }
    
    func    addGroup(groupname: String, users: [User], current: String, id: Int) -> Group {
        let group = Group(context: context!)
        let owner = fetchUser(username: current)
        
        group.creator = owner!.name
        group.gid = Int32(truncatingIfNeeded: id)
        group.date = getDateNowFormatted()
        group.name = groupname
        group.pic = ""
        
        for user in users {
            group.addToUser(user)
            user.addToGroup(group)
        }
        owner!.addToGroup(group)
        appDelegate?.saveContext()
        
        return group
    }

    func    addMessage(group: Group, content: String, sender: String) -> Message{
        let msg = Message(context: context!)
        
        msg.date = getDateNowFormatted()
        msg.from = sender
        msg.id = String(UUID().hashValue)
        msg.msg = content
        msg.to = group.name
        
        msg.group = group
        group.addToMessages(msg)
        
        appDelegate?.saveContext()
        return msg
    }
    
    //MARK: - Update methods
    func    updateGroupsAndMessage(data: Receive.Response) {
        for var elem in data.msg {
            if elem.msg.contains("\"type\":0") {
                handleNewMessage(msg: elem)
            } else {
                handleNewGroup(group: elem)
            }
        }
    }
    

    func    updateUserData(data: UserInfo.Response) {
        let request = NSFetchRequest<User>(entityName: "User")
        request.predicate = NSPredicate(format: "name == %@", data.username)
        
        do {
            let result = try context!.fetch(request)
            if result.count == 1 {
                result[0].publicKey = data.publicKey
                appDelegate?.saveContext()
            } else if result.count == 0 {
                addUser(data: data)
            }
        } catch let error {
            print(error)
        }
    }
    
    // MARK: - Tools
    private func    handleNewGroup(group: MessageFormat) {
        do {
            let content = try JSONDecoder().decode(GroupTypeFormat.self, from: group.msg.data(using: .utf8)!)
            if (fetchGroup(id: Int32(truncatingIfNeeded: content.gid)) != nil) {
                return
            }
            
            var userlist = content.userlist.replacingOccurrences(of: "[", with: "")
            userlist = userlist.replacingOccurrences(of: "]", with: "")
            userlist = userlist.replacingOccurrences(of: "\"", with: "")
            
            let userArray = userlist.components(separatedBy: ",")
            var users: [User] = []

            for user in userArray {
                if let usr = fetchUser(username: user) {
                    users.append(usr)
                } else {
                    let nusr = User(context: context!)
                    nusr.name = user
                    users.append(nusr)
                    appDelegate?.saveContext()
                }
            }
            
            var creator = fetchUser(username: content.creator)
            if creator == nil {
                creator = User(context: context!)
                creator!.name = content.creator
                appDelegate?.saveContext()
            }
            
            addGroup(groupname: content.grpName, users: users, current: content.creator, id: content.gid)
            
            if (creator?.publicKey == nil) {
                SocketIOManager.sharedInstance.askPublicKey(user: creator!.name!)
            }
            for user in users {
                if (user.publicKey == nil){
                    SocketIOManager.sharedInstance.askPublicKey(user: user.name!)
                }
            }
            
        } catch let error {
            print(error)
        }
     
    }
    
    private func    handleNewMessage(msg: MessageFormat) {
        let content = try? JSONDecoder().decode(MessageTypeFormat.self, from: msg.msg.data(using: .utf8)!)

        if (fetchMessageByID(id: msg._id) != nil) {
            return
        }
        
        let group = fetchGroup(id: Int32(truncatingIfNeeded: content!.grpId))
        let message = Message(context: context!)

        message.date = getDateNowFormatted()
        message.from = msg.from
        message.msg = PGPManager.instance.decryptMessage(msg: content!.content)
        message.to = msg.to
        message.id = msg._id
        
        message.group = group
        group?.addToMessages(message)
        message.group = group
        
        appDelegate?.saveContext()
    }
    
    private func    getDateNowFormatted() -> Date {
        let date = Date()
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"

        let str = dateFormat.string(from: date)
        return dateFormat.date(from: str)!
    }
}
