//
//  BiometricsAuthentification.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-12-09.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import Foundation
import LocalAuthentication

class BiometricIDAuth {
    private enum BiometricType {
        case none
        case touchID
        case faceID
    }
    
    let context = LAContext()
    
    var authType = "button"
    var logReason = "Logging in with "
    
    func authenticateUser(completion: @escaping () -> Void) {
        guard canEvaluatePolicy() else {
            return
        }
        
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: logReason + authType) {
            (success, error) in
            if success {
                DispatchQueue.main.async {
                    completion()
                }
            } else {
                print(error)
            }
        }
    }
    
    func biometricType() -> String {
        let _ = context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    
        switch context.biometryType {
        case .touchID:
            authType = "touchID"
        case .faceID:
            authType = "faceID"
        default:
            authType = "none"
        }
        
        return authType
    }
    
    func canEvaluatePolicy() -> Bool {
        return context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    }
}
