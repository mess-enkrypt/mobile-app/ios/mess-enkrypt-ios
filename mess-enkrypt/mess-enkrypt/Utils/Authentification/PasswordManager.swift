//
//  PasswordManager.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-12-09.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import Foundation
import Keychain

class PasswordManager {
    
    private let access = "messenkrypt.messenkrypt-ios"
    
    func storePassword(user: String, pass: String) {
        Keychain.set(password: pass, account: user, service: access)
    }
    
    func retrievePassword(user: String) -> String? {
        if let password = Keychain.get(account: user, service: access) {
            return password
        }
        return nil
    }
    
    func checkPassword(user: String, pass: String) -> Bool {
        let toCompare = self.retrievePassword(user: user)
        if toCompare == nil || toCompare != pass {
            return false
        }
        
        return true
    }
    
    func displayAlert(user: String, view: UIViewController) {
        let alert = UIAlertController(title: "Authentication", message: "Please enter your password", preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textfield)  in
            textfield.placeholder = "password"
        })
        
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0].text ?? ""
            if (self.checkPassword(user: user, pass: textField) == false) {
                self.displayAlert(user: user, view: view)
            }
        }))
        
        view.present(alert, animated: true, completion: nil)
    }
}
