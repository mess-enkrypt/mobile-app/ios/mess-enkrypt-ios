//
//  QRCodeGenerator.swift
//  mess-enkrypt
//
//  Created by Irina on 2019-11-12.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import UIKit
import Foundation


class QRCodeGenerator {
    static let instance = QRCodeGenerator()
    
    private func makeQRImage(content: String) -> CIImage? {
        let data = content.data(using: String.Encoding.ascii)
        
        guard let qr = CIFilter(name: "CIQRCodeGenerator") else {
            return nil
        }
        
        guard let colorFilter = CIFilter(name: "CIFalseColor") else {
            return nil
        }
        
        qr.setValue(data, forKey: "inputMessage")
        qr.setValue("H", forKey: "inputCorrectionLevel")
        
        colorFilter.setValue(qr.outputImage, forKey: "inputImage")
        colorFilter.setValue(CIColor(red: 1, green: 1, blue: 1, alpha: 0), forKey: "inputColor1") // Background white
        colorFilter.setValue(CIColor(red: 155/255, green: 12/255, blue: 67/255), forKey: "inputColor0")
        
        guard let qrImage = colorFilter.outputImage else {
            return nil
        }

        return qrImage
    }
    
    func generate(content: String) -> UIImage? {
        
        let qrCode = makeQRImage(content: content)
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaled = qrCode?.transformed(by: transform)
        
        return UIImage(ciImage: scaled!)
        
    }
}
