//
//  Home_view.swift
//  mess-enkrypt
//
//  Created by Nicolas Cartier on 08/05/2019.
//  Copyright © 2019 messEnkrypt. All rights reserved.
//

import Foundation
import UIKit

class Home_view: UIViewController, UITableViewController ,UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var conv_table_view: UITableView!
    let conv: [String] = ["Tony", "Mike", "John", "Remy"];
    
    //Probleme du override, je pense c'est pour ca que la liste ne s'affiche pas !
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conv.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = conv_table_view.dequeueReusableCell(withIdentifier: "string", for: indexPath)
        

        cell.textLabel?.text = conv[indexPath.row]
        
        return cell
    }
    
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        conv_table_view.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")

       
//        conv_table_view.beginUpdates()
//        conv_table_view.insertRows(at: [IndexPath(row: conv.count-1, section: 0)], with: .automatic)
//        conv_table_view.endUpdates()
//        conv_table_view.reloadData()
        
        // Do any additional setup after loading the view.
    }
    
    
}
